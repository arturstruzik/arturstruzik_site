function whenLoaded() {
    $('#loadingBox').fadeOut(800);
    $('#loading_bg').fadeOut(1000, function(){
        $('footer').fadeIn(1500);
        $('header').fadeIn(1500);
        $('section').fadeIn(1500);
    });
    $('body').css('overflow','visible')
}

function prepareLoadingView(){
    $('footer').hide();
    $('header').hide();
    $('section').hide();
    $('body').css('overflow', 'hidden');
    $('#loading_bg').hide().fadeIn(2000);

    var windowHeight = $(window).height();
    var windowAnimatedPosition = String(parseInt(windowHeight / 3)) + 'px';
    $('#loadingBox').css('top', String(parseInt(windowHeight / 2.5)) + 'px');
    $('#loadingBox').animate({
        top: windowAnimatedPosition,
        opacity: '1'
    },2000);
}

$(document).ready(function () { 
    
    prepareLoadingView(function () {
        isExecutedLoadingPage = true;
    });
});