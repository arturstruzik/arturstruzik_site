var mainPreImgId = 1;
var numberOfPhotos = 11;

$(document).ready(function () {

    $('img').click(function () {
        var img_class = $(this).attr('class');
        if (img_class == "main_img") {
            var url = $(".main_img").attr("src");
            var file_name = String(String(url.match(/\w+[_][0-9]\.jpg$/)).match(/^[A-z]+/)).slice(0, -1);
            window.location.assign("portraits/" + file_name + ".html");
        } else {
            var clicked_img_id_num = parseInt($(this).attr('id').slice(1), 10);
            iterate_id(clicked_img_id_num);
        }
    });

    function iterate_id(clicked_img_id_num) {

        var id_clicked = '#_' + String(clicked_img_id_num);
        var previousMainImg = '#_' + String(mainPreImgId);

        $(previousMainImg).animate({
            left: $(id_clicked).css('left'),
            top: $(id_clicked).css('top'),
            opacity: parseFloat($(id_clicked).css('opacity')),
            'z-index': parseInt($(id_clicked).css('z-index'), 10),
            width: $(id_clicked).css('width')
        }, 1000);

        $(id_clicked).addClass('main_img');
        $(previousMainImg).removeClass('main_img');

        $(id_clicked).animate({
            left: '35%',
            top: '37vh',
            width: '32%',
            opacity: 0.97,
            'z-index': 99
        }, 700);

        var isSaved = false;
        var isLastOne = false;

        for (var k = 1; k <= numberOfPhotos; k++) {
            if (k != mainPreImgId && k != clicked_img_id_num) {

                var _id = '#_' + String(k);
                var _id_next;

                if (!isSaved) {
                    isSaved = true;
                    var _left = $(_id).css('left');
                    var _top = $(_id).css('top');
                    var _opacity = parseFloat($(_id).css('opacity'));
                    var _zIndex = parseInt($(_id).css('z-index'), 10);
                    var _width = $(_id).css('width');
                }

                if (k == numberOfPhotos) {
                    isLastOne = true;
                } else {
                    _id_next = '#_' + String(k + 1);
                    while (k + 1 == mainPreImgId || k + 1 == clicked_img_id_num) {
                        k++;
                        if (k >= numberOfPhotos) {
                            isLastOne = true;
                            break;
                        } else {
                            _id_next = '#_' + String(k + 1);
                        }
                    }
                }

                if (isLastOne) {

                    $(_id).animate({
                        left: _left,
                        top: _top,
                        width: _width,
                        opacity: _opacity,
                        'z-index': _zIndex
                    });
                } else {

                    $(_id).animate({
                        left: $(_id_next).css('left'),
                        top: $(_id_next).css('top'),
                        opacity: parseFloat($(_id_next).css('opacity')),
                        'z-index': parseInt($(_id_next).css('z-index'), 10),
                        width: $(_id_next).css('width')
                    });
                }
            }
        }
        mainPreImgId = clicked_img_id_num;
    }
});