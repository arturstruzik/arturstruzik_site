var isClickedMenu = false;
var isExecutedLoadingPage = false;
var language;
var isRedy = true;

function decreseHeader() {
        $('#logo').stop().animate({
            width: '10vh'
        }, 1000);
        $('header h1').stop().animate({
            paddingTop: '3vh',
            paddingBottom: '3vh',
            'font-size': '0em'
        }, 1000); 
    }

function increaseHeader() {
        $('#logo').stop().animate({
            width: '20vh'
        });
        $('header h1').stop().animate({
            paddingTop: '8vh',
            paddingBottom: '8vh',
            'font-size': '3em'
        }, 900);
    }

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

$(document).ready(function () {

    language = $('html').attr('lang');
    $('#menu_list').hide();
    $('#menu').hide();
    isMobile.any();

    if (language == 'pl') {
        $('img #lang').attr('src', 'graphic/EN.png');
    } else {
        $('img #lang').attr('src', 'graphic/PL.jpg');
    }

    $('header').click(function () {
        isClickedMenu = ~isClickedMenu;
    });

    $('header').stop().hover(
            function () {
                increaseHeader();
                $('#menu_list').stop().fadeIn('normal');
            },
            function () {
                decreseHeader();
                if (!isClickedMenu) {
                    $('#menu_list').stop().fadeOut('slow');
                }
            });
});