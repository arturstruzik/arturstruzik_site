var isMoveBlocked = true;
var photosMoveRight = true;

var portretObject = {

    mousePosition: 0,
    imgOffset: 0,

    init: function () {
        var position = $('.first').offset().left;
        var footerPosition = String($(window).height() - $('footer').height());
        $('section').css('min-height', footerPosition + 'px');
        $('#gradient').css('height', $(window).height() - $('footer').height() + 'px');
        portretObject.imgOffset = $('.first').offset().left;
    },

    setPrePozitions: function () {
        portretObject.mousePosition = event.pageX;
        portretObject.imgOffset = $('.first').offset().left;
    },

    changePhotosPoz: function () {
        var position = 0;

        if ($('.photos').last().offset().left + $('.photos').last().width() < $(window).width() && !photosMoveRight) {
            position = $('.photos').last().offset().left + $('.photos').last().width();
            //debugger;
            $('.first').removeClass('first');
            portretObject.changePhotosOrder();
            $('.photos').first().addClass('first');
            $('.photos').last().css('left', position + 'px');
            portretObject.setPrePozitions();
        }

        if ($('.photos').first().offset().left > 0 && photosMoveRight) {
            position = $('.photos').first().offset().left - $('.photos').last().width();
            $('.photos').first().css('left', position + 'px');
            portretObject.changePhotosOrder();
            portretObject.setPrePozitions();
        }

        var previousImgPosition = 0;
        $('.photos').each(function () {
            var poz = 0;
            poz = event.pageX - portretObject.mousePosition + portretObject.imgOffset;
            poz += previousImgPosition;
            previousImgPosition += parseInt($(this).width());
            $(this).css('left', poz + 'px');
        });
    },

    changePhotosOrder: function () {
        var childNodesOfPhotoAlbum = document.getElementById("photoAlbum").childNodes;
        if (photosMoveRight) {
            document.getElementById("photoAlbum").insertBefore(childNodesOfPhotoAlbum[childNodesOfPhotoAlbum.length - 1], childNodesOfPhotoAlbum[0]);
        } else {
            document.getElementById("photoAlbum").appendChild(childNodesOfPhotoAlbum[0]);
        }

    },

    automaticSlideShow: function () {

        if (isMoveBlocked) {

            photosMoveRight = true;
            var previousImgPosition = 0;
            var position = 0;

            $('.photos').each(function () {

                position = $('.first').offset().left + previousImgPosition + 1;
                previousImgPosition += $(this).width();

                if ($(this).offset().left > $(window).width() && photosMoveRight) {
                    position = $('.first').offset().left - $(this).width();
                    $('.first').removeClass('first');
                    $(this).addClass('first');
                    portretObject.changePhotosOrder();
                }

                $(this).css('left', position + 'px');
            });
        }
    }
}

function startScrollPhotos(){
    isMoveBlocked = false;
}

function stopScrollPhotos(){
    isMoveBlocked = true;
}

$(document).ready(function () {

    portretObject.init();
    isMoveBlocked = true;

    setInterval(portretObject.automaticSlideShow, 25)

    $(window).mousedown(function () {
        portretObject.setPrePozitions();
    });

    $(window).stop().mousemove(function () {
        if (event.pageX - portretObject.mousePosition > 0) {
            photosMoveRight = true;
        } else {
            photosMoveRight = false;
        }

        if (!isMoveBlocked && !isMobile.any()) {
            portretObject.changePhotosPoz();
        }
    });
});